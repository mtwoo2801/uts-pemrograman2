/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.app.uts.npm289.repository;

import org.app.uts.npm289.model.Buku;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



/**
 *
 * @author fadil
 */
@Repository
public interface BukuRepository extends JpaRepository<Buku, String>{
    
}
