package org.app.uts.npm289;

import java.util.List;
import org.app.uts.npm289.model.Buku;
import org.app.uts.npm289.repository.BukuRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ProjectUts43A87006180289Application {

	public static void main(String[] args) {
		SpringApplication.run(ProjectUts43A87006180289Application.class, args);
	}
        
        public CommandLineRunner tesViewBuku (BukuRepository repo){
           return a ->{
               List<Buku> list = repo.findAll();
               System.out.format("%-15s, %-40s, %-30s, %-20s, %-10s \n","ID Buku","Judul","Penagrang","Penerbit","Tahun");
                System.out.println("--------------------------------------------------------------------------------");
                list.forEach(c ->{
                System.out.format("%-15s, %-40s, %-30s, %-20s, %-10s \n",c.getIdBuku(),c.getJudul(),
                        c.getPengarang(),c.getPenerbit(),c.getTahun());
                });
            };
        }
        @Bean
        public CommandLineRunner tesInsertBuku(BukuRepository repo){
            return a ->{
                try {
                Buku buk = new Buku();
                buk.setIdBuku("AAAAAD");
                buk.setJudul("Numeric");
                buk.setPengarang("Erlangga");
                buk.setPenerbit("Dunia Matematika");
                buk.setTahun(2000);
                
                
                repo.save(buk);
                System.out.println("Save Buku Berhasil!");
                } catch (Exception e) {
                    System.out.println("Save Buku Gagal!");
                    System.out.println("Error "+e.getMessage());
                }
            };
        }
        
        public CommandLineRunner tesUpdateBuku(BukuRepository repo){
            return a ->{
                Buku buk = repo.findById("AAAAAC").orElse(null);
                buk.setJudul("Pemrograman Java");
                repo.save(buk); 
                System.out.println("Customer AABAU ditemukan ");
            };
        }
        
        public CommandLineRunner tesDeleteBuku(BukuRepository repo){
            return a ->{
                Buku buk = repo.findById("AAAAAC").orElse(null);
                repo.delete(buk);
                System.out.println("Customer AABAD sudah dihapus ");
            };
        }
        
        public CommandLineRunner testGetById(BukuRepository repo){
            return a ->{
                Buku buk = repo.findById("AAAAAC").orElse(null);
                System.out.format("%-15s, %-40s, %-30s, %-20s, %-10s \n","ID Buku","Judul","Penagrang","Penerbit","Tahun");
                System.out.println("--------------------------------------------------------------------------------");
                System.out.format("%-15s, %-40s, %-30s, %-20s, %-10s \n",buk.getIdBuku(),buk.getJudul(),
                        buk.getPengarang(),buk.getPenerbit(),buk.getTahun());
            };
        }

}
